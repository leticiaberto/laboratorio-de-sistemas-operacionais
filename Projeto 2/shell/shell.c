#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <sys/wait.h>

#define MAX 256

int main() 
{
	char comando[MAX];
	int pid,flag, j, i, flagme, flagma;
	char *token, *argv[MAX];
	
	while (1) 
	{
		printf("> ");
	
		flag = 0;
		flagme = -1;
		flagma = -1;
		j = 0;
	
		fgets(comando, MAX, stdin);
		while(comando[j] != '\n')
			j++;
		comando[j] = '\0';
	
		token = (char*)strtok(comando, " ");
		
		i = 0;
		while (token != NULL)
		{
			argv[i] = token;
			if (!strcmp(argv[i], "<"))
			{	
				flagme = i;
				i--;
			}
			if (!strcmp(argv[i], ">")) 
			{	
				flagma = i;
				i--;
			}
			token = (char*)strtok(NULL, " ");
			i++;
		}
		argv[i] = NULL;

		if (i != 0) //evitar falha de segmentação (i < 0)
		{
			if (!strcmp(argv[0], "exit")) 
	 			exit(EXIT_SUCCESS);

			if (!strcmp(argv[i-1], "&"))
			{	
				flag = 1;
				argv[i-1] = NULL;
			}
		}
	
		pid = fork();	//fork retorna 0 para o pai quando o processo foi criado. Cria uma cópia desse código para o filho que executa ao mesmo tempo que o pai
		if (pid)
		{		
			if (flag != 1)      
				waitpid(pid, NULL, 0); //espera término do processo (se sou pai)
		} 
		else 
		{ 
			if (flagma > 0)
			{
				freopen(argv[flagma], "w", stdout);
				argv[flagma] = NULL;
			}
			if (flagme > 0)
			{
				freopen(argv[flagme], "r", stdin);
				argv[flagme] = NULL;
			}
			execvp(argv[0], argv);	//tem o próprio exit dele
			printf("Erro ao executar comando!\n");	//a partir daqui só é executado se o execvp der errado	
			exit(EXIT_FAILURE);
		}
	}
}
