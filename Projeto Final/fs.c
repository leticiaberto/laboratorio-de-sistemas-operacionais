/*
 * RSFS - Really Simple File System
 *
 * Copyright © 2010 Gustavo Maciel Dias Vieira
 * Copyright © 2010 Rodrigo Rocco Barbieri
 *
 * This file is part of RSFS.
 *
 * RSFS is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

 /* Integrantes: Elisa Marques de Castro    587303
                 Letícia Mara Berto         587354
                 Wesley Moura Rocha         587311
                 William Barom Mingardi     587362
*/
#include <stdio.h>
#include <string.h>
#include <limits.h>

#include "disk.h"
#include "fs.h"

#define CLUSTERSIZE 4096


#define FREE_CLUSTER 1
#define LAST_CLUSTER 2
#define FAT_CLUSTER 3
#define DIR_CLUSTER 4

#define FREE 0
#define USED 1
//Define flag que indica que o arquivo está fechado
#define FS_CLOSED -1

unsigned short fat[65536];

typedef struct {
       char used;
       char name[25];
       unsigned short first_block;
       int size;
} dir_entry;

dir_entry dir[128];

// Struct que armazena os dados de um arquivo aberto
typedef struct {
    // Indica em qual modo o arquivo está aberto
    char status;
    // Indica o cluster que o ponteiro está
    unsigned short currentCluster;
    // Indica quantos bytes estão no cluster
    int currentByte;
    // Indica quantos bytes foram percorridos no total
    int currentByteTotal;
} filePointer;

// Vetor de apontadores de arquivo
filePointer fpointer[128];

// Função auxiliar
// Escreve as modificações da FAT e do DIR no disco
// retornando 1 caso a escrita foi um sucesso.
int save_changes(){
    char *buffer;
    int sizeTemp, i, j;

    // Feito o cast para tratar o vetor fat como um vetor de caracter
    // para pode-lo escrever no disco.
    buffer = (char *) fat;

    // Escreve no disco a FAT
    // Observação: O interador i incrementa em SECTORSIZE
    // que é o tamanho de um setor
    sizeTemp = sizeof(fat);
    for(i = 0, j = 0; i < sizeTemp; i += SECTORSIZE, j++){
        if( !bl_write(j, &buffer[i]) ){
            return 0;
        }
    }

    // Feito o cast para tratar o vetor dir como um vetor de caracter
    // para pode-lo escrever no disco.
    buffer = (char *) dir;

    //Escreve o diretório no disco
    sizeTemp = sizeof(dir);
    for(i = 0; i < sizeTemp; i += SECTORSIZE, j++){
        if( !bl_write(j, &buffer[i]) ){
            return 0;
        }
    }
    return 1;
}


int fs_init() {
    char *buffer;
    int i, j, sizeTemp;


    // Feito o cast para tratar o vetor fat como um vetor de caracter
    // para poder ler-lo no disco.
    buffer = (char *) fat;

    // Carrega do disco a FAT
    // Observação: O interador i incrementa em SECTORSIZE
    // que é o tamanho de um setor
    sizeTemp = sizeof(fat);
    for(i = 0, j = 0; i < sizeTemp; i += SECTORSIZE, j++){
        if( !bl_read(j, &buffer[i]) ){
            return 0;
        }
    }

    //Verifica se o disco está formatado analizando o vetor FAT carregado.
    for(i = 0; i < 32; i++){
        // Caso uma das 32 posições reservadas do FAT não
        // possuir a flag FAT_CLUSTER que indica uma posição
        // FAT o disco não está formatado.
        if(fat[i] != FAT_CLUSTER){
            printf("Formatando...\n\n");
            if(!fs_format()){
                perror("Erro ao formatar o disco\n");
                return 0;
            }
            return 1;
        }
    }

    buffer = (char *) dir;

    // Carrega o diretório na memória.
    sizeTemp = sizeof(dir);
    for(i = 0; i < sizeTemp; i += SECTORSIZE, j++){
        if( !bl_read(j, &buffer[i]) ){
            return 0;
        }
    }

    // Inicializa todos os arquivos como fechados
    for(i = 0; i < 128; i++){
        fpointer[i].status = FS_CLOSED;
    }

  return 1;
}

int fs_format() {
    int i;

    // Inicia os agrupamentos com um valor especial, indicando
    // que este agrupamento está sendo usado pela FAT.
    for(i = 0; i < 32; i++){
        fat[i] = FAT_CLUSTER;
    }

    // Inicia o agrupamento 32 com um valor especial, indicando
    // que este agrupamento é reservado para o diretorio.
    fat[32] = DIR_CLUSTER;

    // Inicia o resto dos agrupamentos como vazio.
    for(i = 33; i < 65536; i++){
        fat[i] = FREE_CLUSTER;
    }

    // Inicializa todos os arquivos do diretório como vazio.
    // E inicializa todos os arquivos como fechado
    for(i = 0; i < 128; i++){
        dir[i].used = FREE;
        fpointer[i].status = FS_CLOSED;
    }

    // Salva as alterações no disco caso ocorra um erro
    // retorne erro (0)
    if(!save_changes()){
        return 0;
    }

  return 1;
}

int fs_free() {
    int size, i;
    int numOfClusters;

    size = 0;
    // Calcula o numero de clusters existentes no disco.
    // Observação: Cada cluster ocupa 8 setores.
    numOfClusters = bl_size() / 8;

    // Verifica em cada posição da FAT se existe o indicador
    // especial para cluster vazio, se sim soma o tamanho de um
    // cluster em size.
    // Observação: O interador começa em 33 pois as poisições
    // 0 - 32 são reservadas para o sistema de arquivos.
    for(i = 33; i < numOfClusters; i++){
        if(fat[i] == FREE_CLUSTER){
            size += 4096;
        }
    }

    return size;
}

int fs_list(char *buffer, int size) {
    int i;
    char tempBuffer[256];
    *buffer = '\0';
    for( i = 0; i < 128; i++){
        if(dir[i].used == USED){
            sprintf(tempBuffer, "%s\t\t%d\n", dir[i].name, dir[i].size);

            // Caso o tamanho do buffer for ficar maior que o seu tamanho máximo (size) retorne um erro.
            if( (strlen(buffer) + strlen(tempBuffer)) >=  size){
                perror("Overflow do buffer fornecido\n");
                return 0;
            }else{
                strcat(buffer, tempBuffer);
            }
        }
    }
  return 1;
}

int fs_create(char* file_name) {
    int firstBlock, i;
    int numOfClusters;

    // Verifica se o tamanho do arquivo não ultrapassa
    // o tamanho maximo.
    if(strlen(file_name) > 24){
        perror("Tamanho de arquivo muito grande (Max: 24).\n");
        return 0;
    }

    // Calcula o numero de clusters existentes no disco.
    // Observação: Cada cluster ocupa 8 setores.
    numOfClusters = bl_size() / 8;

    //Encontra o primeiro bloco vazio.
    for(firstBlock = 33; firstBlock < numOfClusters ; firstBlock++){
        if(fat[firstBlock] == FREE_CLUSTER){
            fat[firstBlock] = LAST_CLUSTER;
            break;
        }
    }

    // Se firstBlock for igual a numOfClusters ignifica que toda
    // FAT foi percorrida e não foi encontrado um agrupamento vazio.
    if(firstBlock == numOfClusters){
        perror("Não foi possível cirar o arquivo pois o disco está cheio.\n");
        return 0;
    }

    // Verifica se o nome já esta sendo usado.
    for(i = 0; i < 128; i++){
        if(dir[i].used == USED){
            if(strcmp(file_name, dir[i].name) == 0){
                fprintf(stderr, "O nome de arquivo %s já esta sendo usado.\n", file_name);
                return 0;
            }
        }
    }

    // Procura a primeira posição vazia e preenche com seus dados.
    for(i = 0; i < 128; i++){
        if(dir[i].used == FREE){
            strcpy(dir[i].name, file_name);
            dir[i].used = USED;
            dir[i].size = 0;
            dir[i].first_block = firstBlock;
            break;
        }
    }

    // Se não encontrou uma posição vazia, então o diretório está cheio.
    if(i == 128){
        perror("Não foi possivel criar o arquivo pois foi atingido o limite máximo de arquivo por diretório (Max: 128).\n");
        return 0;
    }

    // Salva as alterações no disco caso ocorra um erro
    // retorne erro (0)
    if(!save_changes()){
        return 0;
    }

    return 1;
}

int fs_remove(char *file_name) {
    int i;
    unsigned short block, aux;
    // Verifica se o tamanho do arquivo não ultrapassa
    // o tamanho maximo.
    if(strlen(file_name) > 24){
        perror("Tamanho de arquivo muito grande (Max: 24).\n");
        return 0;
    }

    // Procura o arquivo a ser removido
    for(i = 0; i < 128; i++){
        if(dir[i].used == USED){
            if(strcmp(file_name, dir[i].name) == 0){
                break;
            }
        }
    }

    // Se o i for igual a 128 o dir foi todo percorrido e o nome
    // não foi encontrado.
    if(i == 128){
        fprintf(stderr, "O arquivo %s não existe.\n", file_name);
        return 0;
    }

    block = dir[i].first_block;
    dir[i].used = FREE;
    fpointer[i].status = FS_CLOSED;
    aux = 0;
    // Percorre o encadeamento do arquivo na FAT
    // liberando seus agrupementos.
    while(aux != LAST_CLUSTER){
        aux = fat[block];
        fat[block] = FREE_CLUSTER;
        block = aux;
    }

    // Salva as alterações no disco caso ocorra um erro
    // retorne erro (0)
    if(!save_changes()){
        return 0;
    }

    return 1;
}

int fs_open(char *file_name, int mode) {

    int i;
    // Verifica se o nome do arquivo a ser aberto ultrapassa
    // o limite permitido.
    if(strlen(file_name) > 24){
        perror("Tamanho de arquivo muito grande (Max: 24).\n");
        return -1;
    }

    // Se for aberto para leitura
    if(mode == FS_R){

        // Procura o arquivo a ser aberto e inicializa
        // seu apontador de arquivo
        for(i = 0; i < 128; i++){
            if(dir[i].used == USED){
                if(strcmp(file_name, dir[i].name) == 0){
                    fpointer[i].status = FS_R;
                    fpointer[i].currentCluster = dir[i].first_block;
                    fpointer[i].currentByte = 0;
                    fpointer[i].currentByteTotal = 0;
                    return i;
                }
            }
        }

        perror("O arquivo que deseja ser aberto não existe.\n");

        return -1;

    // Se for aberto para escrita
    } else if(mode == FS_W){

        // Procura o arquivo a ser aberto e o apague se existir.
        for(i = 0; i < 128; i++){
            if(dir[i].used == USED){
                if(strcmp(file_name, dir[i].name) == 0){
                    if(!fs_remove(file_name)){
                        perror("Erro ao excluir o arquivo existente.");
                        return -1;
                    }
                    break;
                }
            }
        }

        // Tenta criar o arquivo
        if(!fs_create(file_name)){
            perror("Ocorreu um erro ao criar o arquivo.\n");
            return -1;
        }

        // Procura o arquivo a ser aberto e inicializa
        // seu apontador de arquivo
        for(i = 0; i < 128; i++){
            if(dir[i].used == USED){
                if(strcmp(file_name, dir[i].name) == 0){
                    fpointer[i].status = FS_W;
                    fpointer[i].currentCluster = dir[i].first_block;
                    fpointer[i].currentByte = 0;
                    fpointer[i].currentByteTotal = 0;
                    return i;
                }
            }
        }
    // Caso seja um modo inváalido
    }else{
        perror("O arquivo deve ser aberto de uma maneira válida.\n");
        return -1;
    }

    return 1;
}

int fs_close(int file){

    // Verifica se o arquvivo está fechado
    if(fpointer[file].status == FS_CLOSED){
        perror("Não existe arquivo aberto com este indentificador.\n");
        return 0;
    }

    // Escreve as alterações da FAT e DIR no disco
    if(!save_changes()){
        return 0;
    }

    // Fecha o ponteiro do arquivo
    fpointer[file].status = FS_CLOSED;
    return 1;
}

int fs_write(char *buffer, int size, int file) {
    char clusterTemp[CLUSTERSIZE];
    int i, j, k;
    int bytesW, numOfClusters, freeBlock;

    // Verifica se o arquivo foi aberto corretamente
    if(fpointer[file].status == FS_CLOSED){
        perror("Não existe arquivo aberto com este indentificador.\n");
        return -1;
    }
    // Verifica se o arquivo foi aberto corretamente
    if(fpointer[file].status == FS_R){
        perror("O arquivo foi aberto para leitura.\n");
        return -1;
    }

    // Verifica se o tamanho limite do arquivo é ultrapassado
    if(size + dir[file].size > INT_MAX){
        perror("Não é possivel fazer esta escrita pois o tamanho máximo de arquivo será ultrapassado.\n");
        return -1;
    }

    // Calcula o numero de cluster existentes
    numOfClusters = bl_size() / 8;


    //calcula a posição do primeiro setor do cluster atual
    j = fpointer[file].currentCluster * 8;
    //Carrega o  cluster na memória
    for(i = 0, k = 0; k < 8; i += SECTORSIZE, j++, k++){
        if( !bl_read(j, &clusterTemp[i]) ){
            return -1;
        }
    }


    // Numero de bytes escritos
    bytesW = 0;
     while(1){
        // Enquanto o cluster NÃO foi preenchido
        // E size for maior de 0
         while(fpointer[file].currentByte < CLUSTERSIZE && size > 0){
            // preenche a cluster com dados
            clusterTemp[fpointer[file].currentByte++] = buffer[bytesW++];
            //aumenta o tamanho do arquivo
            dir[file].size++;
            // Decrementa size, que é o quanto o usuário que escrever
            size--;
         }

        // Caso foi possivel escrever tudo no cluster
        // termina a escrita
        if(size == 0){
            // Escreve o buffer no setor
            j = fpointer[file].currentCluster * 8;
            for(i = 0, k = 0; k < 8; i += SECTORSIZE, j++, k++){
                if( !bl_write(j, &clusterTemp[i]) ){
                    return -1;
                }
            }
            return bytesW;
        // Não foi possivel escrever tudo no cluster atual logo
        // é necessário alocar mais um cluster para o arquvivo.
        }else{
            //Escreve as alterações do cluster atual
            j = fpointer[file].currentCluster * 8;
            for(i = 0, k = 0; k < 8; i += SECTORSIZE, j++, k++){
                if( !bl_write(j, &clusterTemp[i]) ){
                    return -1;
                }
            }

            //Encontra o primeiro bloco vazio para aloca-lo.
            for(freeBlock = 33; freeBlock < numOfClusters ; freeBlock++){
                if(fat[freeBlock] == FREE_CLUSTER){
                    //Faz o cluster atual apontar para a cluster vazia
                    fat[fpointer[file].currentCluster] = freeBlock;
                    // Inicializa a cluster vazia como ultima
                    fat[freeBlock] = LAST_CLUSTER;
                    //Atualiza o cluster atual como a cluster vazia
                    fpointer[file].currentCluster = freeBlock;
                    //atualiza o ponteiro de byte da cluster para 0
                    fpointer[file].currentByte = 0;
                    break;
                }
            }
            // Se freeBlock for igual a numOfClusters ignifica que toda
            // FAT foi percorrida e não foi encontrado um agrupamento vazio.
            if(freeBlock == numOfClusters){
                perror("Não foi possível escrever mais no disco pois ele está cheio.\n");
                return -1;
            }
        }
     }

  return -1;
}

int fs_read(char *buffer, int size, int file) {
    char clusterTemp[CLUSTERSIZE];
    int i, j, k;
    int bytesR;

    // Verifica se o arquivo foi aberto corretamente
    if(fpointer[file].status == FS_CLOSED){
        perror("Não exister arquivo aberto com este indentificador.\n");
        return -1;
    }
    // Verifica se o arquivo foi aberto corretamente
    if(fpointer[file].status == FS_W){
        perror("O arquivo foi aberto para escrita.\n");
        return -1;
    }

    //calcula a posição do primeiro setor do cluster atual
    j = fpointer[file].currentCluster * 8;

    //Carrega o  cluster na memória
    for(i = 0, k = 0; k < 8; i += SECTORSIZE, j++, k++){
        if( !bl_read(j, &clusterTemp[i]) ){
            return -1;
        }
    }


    //Numero de bytes lidos
    bytesR = 0;
     while(1){
        // Enquanto o cluster NÃO foi completamente lido
        // ou size for maior de 0 E o currentByteTotal
        // não for mair que o tamanho total do arquivo
         while(fpointer[file].currentByte < CLUSTERSIZE && size > 0 && fpointer[file].currentByteTotal < dir[file].size){
            // preenche o buffer com os dados
            buffer[bytesR++] = clusterTemp[fpointer[file].currentByte++];
            size--;
            fpointer[file].currentByteTotal++;
         }

        // Deu para ler tudo no cluster OU chegou no tamanho
        // máximo do arquivo
        if(size == 0 || fpointer[file].currentByteTotal == dir[file].size){
            return bytesR;
        // Não foi possivel ler tudo no cluster, é necessário procurar
        // o próximo encadiamento
        }else{
            // Aponta para o proximo encadeamento
            fpointer[file].currentCluster = fat[fpointer[file].currentCluster];
            //atualiza o ponteiro de byte da cluster para 0
            fpointer[file].currentByte = 0;

            //calcula a posição do primeiro setor do cluster atual
            j = fpointer[file].currentCluster * 8;
            //Carrega o  cluster na memória
            for(i = 0, k = 0; k < 8; i += SECTORSIZE, j++, k++){
                if( !bl_read(j, &clusterTemp[i]) ){
                    return -1;
                }
            }
        }

    }

    return -1;
}
