#define _GNU_SOURCE
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <crypt.h>
#include <pthread.h>
#include <unistd.h>
#include <semaphore.h>

#define PASS_SIZE 4
#define HASH_SIZE 256
#define NUM_CONSUMIDORES 2

typedef struct fila {
  char password[10];
  struct fila *prox;
} Fila;

pthread_t produtor;
pthread_t consumidor[NUM_CONSUMIDORES]; // Vetor de threads consumidoras
char hash[HASH_SIZE]; // Hash que está sendo procurada
Fila *filaPasswords; // Fila de senhas já geradas
sem_t sem; // Semáforo que regula o acesso à fila de senhas geradas

int found; // Flag que indica que a hash procurada foi encontrada
int no_more_pass; // Flag que indica que o produtor já produziu todas as possiveis senhas

Fila *initFila(Fila *f);
Fila *insertFila(Fila *f, char password[10]);
char *getFirstElementFila(Fila *f);
Fila *removeFirstElementFila(Fila *f);
void freeFila(Fila *f);

int getNextPassword(char *password) {
  int index = PASS_SIZE - 1;

  while (index >= 0) {
    if (password[index] != 'z') {
      password[index]++;
      break;
    }
    if (index == 0) {
      return 0;
    }
    password[index] = 'a';
    index--;
  }

  return 1;
}

// Função usada pela thread produtora
void *producePasswords(void *v) {

  char pass[10];
  int i;

  for (i = 0; i < PASS_SIZE; i++) {
    pass[i] = 'a';
  }
  pass[i] = '\0';

  // Loop que roda enquanto for possivel gerar novas senhas
  do {

    // Se alguma consumidora já encontrou a senha desejada, não produz mais senhas
    if (found) {
      return NULL;
    }

    // Bloqueia o semáforo que regula o acesso à fila de senhas geradas
    sem_wait(&sem);

    //printf("insere %s\n", pass);
    filaPasswords = insertFila(filaPasswords, pass);

    // Libera o semáforo das filas
    sem_post(&sem);
    usleep(1);
  } while (getNextPassword(pass));


  no_more_pass = 1;


  return NULL;
}


// Função usada pelas threads consumidoras
void *testPasswords(void *v) {

  char *pass;
  char testHash[HASH_SIZE];

  struct crypt_data cdata;
  cdata.initialized = 0;

  do {

    // Se a senha já foi bloqueada, encerra a thread
    if (found) {
      return NULL;
    }

    // Bloqueia o semáforo de acesso à fila
    sem_wait(&sem);

    pass = getFirstElementFila(filaPasswords);

    // Se a fila estava vazia e não há mais senhas a serem produzidas, encerra
    if (pass == NULL) {
      if (no_more_pass) {

        // Libera o semáforo antes de finalizar
        sem_post(&sem);
        return NULL;
      }
    } else {
      filaPasswords = removeFirstElementFila(filaPasswords);
    }

    // Libera o semáforo
    sem_post(&sem);

    if (pass != NULL) {

      // Gera o hash da senha que está sendo testada
      strcpy(testHash, crypt_r(pass, "aa", &cdata));

      // Compara a hash gerada com a que está sendo procurada
      if (strcmp(hash, testHash) == 0) {
        printf("Achou: %s\n", pass);
        found = 1;
        return NULL;
      }
    }

  } while (1);

  return NULL;
}


int main(int argc, char *argv[]) {

  int i;
  found = 0;
  no_more_pass = 0;

  strcpy(hash, argv[1]);

  filaPasswords = initFila(filaPasswords);

  // Inicialização do semáforo
  sem_init(&sem, 0, 1);

  // Criação das threads
  pthread_create(&produtor, NULL, producePasswords, NULL);
  for (i = 0; i < NUM_CONSUMIDORES; i++) {
    pthread_create(&consumidor[i], NULL, testPasswords, NULL);
  }

  // Aguarda a finalização das threads
  pthread_join(produtor, NULL);
  for (i = 0; i < NUM_CONSUMIDORES; i++) {
    pthread_join(consumidor[i], NULL);
  }

  if (!found) {
    printf("Senha não encontrada!\n");
  }

  freeFila(filaPasswords);
  return 0;
}


Fila *initFila(Fila *f) {
  f = NULL;

  return f;
}

Fila *insertFila(Fila *f, char password[10]) {
  Fila *aux, *new;

  new = (Fila *)malloc(sizeof(Fila));
  strcpy(new->password, password);
  new->prox = NULL;

  if (f == NULL) {
    f = new;
  } else {
    aux = f;
    while (aux->prox != NULL) {
      aux = aux->prox;
    }
    aux->prox = new;
  }

  return f;
}

char *getFirstElementFila(Fila *f) {

  if (f == NULL) {
    return NULL;
  }

  char *str = (char *) malloc(sizeof(char) * 10);
  strcpy(str, f->password);

  return str;
}

Fila *removeFirstElementFila(Fila *f) {
  Fila *aux;

  if (f != NULL) {
    aux = f;
    f = aux->prox;
    free(aux);
  }

  return f;
}

void freeFila(Fila *f) {
  Fila *delete, *aux;
  aux = f;

	while (aux != NULL) {
    //printf("freeing %s\n", aux->password);
		delete = aux;
		aux = aux->prox;
		free (delete);
	}
}
